SET client_encoding = 'UTF8';

-- -------------------------------------------------
-- DB作成
-- -------------------------------------------------
CREATE DATABASE dev OWNER 'dbadmin' encoding 'UTF8' lc_collate 'ja_JP.UTF-8' lc_ctype 'ja_JP.UTF-8' template 'template0';

--REVOKE CREATE ON SCHEMA public FROM PUBLIC;
--REVOKE ALL ON DATABASE dev FROM PUBLIC;

-- -------------------------------------------------
-- ユーザ作成
-- -------------------------------------------------
\c dev;

-- ユーザ作成
CREATE USER batuser WITH PASSWORD 'batpass' LOGIN;
CREATE USER devuser WITH PASSWORD 'devpass' LOGIN;

SHOW search_path;

\dn

-- -------------------------------------------------
-- スキーマ作成
-- -------------------------------------------------
CREATE SCHEMA petstore;

\dn

-- -------------------------------------------------
-- 権限付与
-- -------------------------------------------------
GRANT USAGE ON SCHEMA petstore TO batuser;
GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER ON ALL TABLES IN SCHEMA  petstore TO batuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA  petstore GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER ON TABLES TO batuser;


GRANT USAGE ON SCHEMA petstore TO devuser;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA  petstore TO devuser;
ALTER DEFAULT PRIVILEGES IN SCHEMA  petstore GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO devuser;

-- -------------------------------------------------
-- search_path設定
-- -------------------------------------------------
ALTER USER dbadmin SET search_path TO "$user", public, petstore;
ALTER USER batuser SET search_path TO "$user", public, petstore;
ALTER USER devuser SET search_path TO "$user", public, petstore;

